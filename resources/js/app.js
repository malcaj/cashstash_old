import Vue from 'vue'
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';

Vue.use(VueRouter)

Vue.use(VueResource);

Vue.component('chart', require('./components/Chart.vue'));
Vue.component('alter', require('./components/Alter.vue'));

window.Vue = require('vue');

window.axios = require('axios');

window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
    'X-Requested-With': 'XMLHttpRequest'
};

const app = new Vue({
    el: '#app',
    VueRouter,
});