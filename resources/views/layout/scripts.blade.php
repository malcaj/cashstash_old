<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="{{ asset('vendor/js/Chart.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}" defer></script>

@stack('scripts')